import { action, computed, observable } from "mobx"

export default class Store {
  @observable boxSize = 130;
  @observable cubesize = 5;
  @observable xyf = false;
  @observable report = false;

  @action setCubeSize = (newcubesize) => {
    this.cubesize = newcubesize;
  }
  @action setXYF = (newxyf) => {
    const x = newxyf[0];
    const y = newxyf[1];
    const f = newxyf[2];
    const ismove = ((x<=this.cubesize-1 && y<=this.cubesize-1) && (x>=0 && y>=0)) ? true:false;
    if(ismove) {
      this.xyf = newxyf;
    } else {console.log('moven not allowed')}
  }
  @action setPlace = (xyf) => {
    const direction = {
      'NORTH': 'N',
      'EAST': 'E',
      'SOUTH': 'S',
      'WEST': 'W',
    }
    xyf[2] = (xyf[2] in direction) ? direction[xyf[2]]: xyf[2];
    this.setXYF([x,y,f]);
  }
  @action setDirection = (direction) => {
    // lef and right directions
    const leftRotation = ['N', 'W', 'S', 'E'];
    const rightRotation = ['N', 'E', 'S', 'W'];

    let x = this.xyf[0];
    let y = this.xyf[1];
    let f = this.xyf[2];

    let pos = false;
    if(direction==='L') {
      let lindex = ((leftRotation.indexOf(f) + 1) < leftRotation.length) ? leftRotation.indexOf(f) + 1 : 0;
      pos = leftRotation[lindex];
    } else {
      let rindex = ((rightRotation.indexOf(f) + 1) < rightRotation.length) ? rightRotation.indexOf(f) + 1 : 0;
      pos = rightRotation[rindex];
    }
    f = pos;
    this.setXYF([x,y,f]);
  }
  @action setMove = () => {
    // x axis as west and east
    const xmove = ['W', 'E']
    // y axis as north and south
    const ymove = ['N','S'];
    // move operation classification
    const addmove = ['N', 'E'];
    const submove = ['S', 'W'];
    let x = this.xyf[0];
    let y = this.xyf[1];
    let f = this.xyf[2];

    let isxmove = xmove.indexOf(f) > -1 ? true:false;
    let isadd = addmove.indexOf(f) > -1 ? true:false;
    if (isxmove) {
      if (isadd) {
        x++;
      } else { x--; }
    } else {
      if (isadd) {
        y++;
      } else { y--; }
    }
    this.setXYF([x,y,f]);
  }
  @action setReport = () => {
    const direction = {
      'N': 'NORTH',
      'E': 'EAST',
      'S': 'SOUTH',
      'W': 'WEST',
    }
    let x = this.xyf[0];
    let y = this.xyf[1];
    let f = direction[this.xyf[2]];
    let report = x + ' ' + y + ' ' + f;
    this.report = report;
  }

}
