import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { Provider } from 'mobx-react';

// routes
import MainRoute from './routes/MainRoute';
// stores
import Stores from './store/Stores';

const applicationContainer = document.getElementById("application");

ReactDOM.render(
    <Provider {...Stores}>
      <BrowserRouter>
        <MainRoute />
      </BrowserRouter>
    </Provider>,
  applicationContainer
);
