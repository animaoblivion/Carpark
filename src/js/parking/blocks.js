import React from "react"

export default class Index extends React.Component {

  render() {
    const boxSize = this.props.boxSize;
    const x = this.props.x;
    const y = this.props.y;
    const style = {
      backgroundColor: 'white',
      color: 'red',
      width: boxSize + 'px',
      height: boxSize + 'px',
      float: 'left',
      position: 'relative'
    };
    return <div style={style}></div>
  }

}
