import React from "react"

export default class Index extends React.Component {

  render() {
    const xyf = this.props.xyf;
    const boxSize = this.props.boxSize;
    const left = xyf[0] * boxSize;
    const bottom = xyf[1] * boxSize;
    const face = xyf[2]
    const style = {
      color: 'green',
      position: 'absolute',
      left: left + 'px',
      bottom: bottom + 'px',
      zIndex: 999999,
    }
    const cls = 'bus bus-' + face;
    return <div className={cls} style={style}></div>
  }

}
