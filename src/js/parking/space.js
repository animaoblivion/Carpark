import React from "react"
import { inject, observer } from 'mobx-react';

import Carpark from './carpark';
import Bus from './bus';

@inject('parkingStore') @observer
export default class Index extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      commandLine: ''
    };
  }

  componentDidMount = () => {
    this.props.parkingStore.setXYF([0,0,'N']);
  }

  handlePlace = (xyf) => {
    this.props.parkingStore.setPlace(xyf);
  }

  handleMove = () => {
    this.props.parkingStore.setMove();
  }

  handleDirection = (direction) => {
    this.props.parkingStore.setDirection(direction);
  }

  handleReport = () => {
    this.props.parkingStore.setReport();
  }

  handleRun = () => {
    const commands = this.state.commandLine;
    const command = commands.split('\n');

    for(let i=0;i<command.length;i++) {
      let line = command[i].split(' ');
      let action = line[0]
      if (i===0 && line[0] !== 'PLACE') {
        return false;
      }
      switch (action) {
          case 'PLACE':
              let xyf = line[1].split(',');
              this.handlePlace(xyf);
              break;
          case 'MOVE':
              this.handleMove();
              break;
          case 'LEFT':
              this.handleDirection('L');
              break;
          case 'RIGHT':
              this.handleDirection('R');
              break;
          case 'REPORT':
              this.handleReport();
              break;
      }

    }

  }

  handleOnChangeCommandLine = (evt) => {
    this.setState({
     commandLine: evt.target.value
    });
  }

  render() {
    const borderSize = 1;
    const boxSize = this.props.parkingStore.boxSize;
    const cubeSize = this.props.parkingStore.cubesize;
    const containerSize = cubeSize * boxSize + (borderSize * 2);
    const style = {
      width: containerSize + 'px',
      height: containerSize + 'px',
      border: borderSize + 'px solid #ccc',
      position: 'relative'
    };

    // bus position
    const xyf = this.props.parkingStore.xyf;
    const report = this.props.parkingStore.report;

    return <div className="carpark">

      <div style={style}>
        <Bus boxSize={boxSize} xyf={xyf} />
        <Carpark boxSize={boxSize} cubeSize={cubeSize} />
      </div>

      <div>
        <br/>
        <button onClick={this.handleMove.bind()} type="button" className="btn btn-primary">Move</button> &nbsp;
        <button onClick={this.handleDirection.bind(this, 'W')} type="button" className="btn btn-primary">Left</button>  &nbsp;
        <button onClick={this.handleDirection.bind(this, 'E')} type="button" className="btn btn-primary">Right</button>  &nbsp;
        <button onClick={this.handleReport.bind()} type="button" className="btn btn-primary">Report</button> &nbsp;
        <span>{report}</span>
      </div>

      <div>
        <br/>
        <textarea id="commandLine" value={this.state.commandLine} onChange={this.handleOnChangeCommandLine.bind(this)}>{this.state.commandLine}</textarea> <br/>
        <button type="button" className="btn btn-primary" onClick={this.handleRun.bind(this)}>Run</button>
      </div>

    </div>
  }

}
