import React from "react"

import Blocks from './blocks';

export default class Index extends React.Component {

  cube = (arraySize, boxSize) => {
    let size = arraySize;
    let multi = new Array(size);
    for (let i=0;i<multi.length;i++) {
      multi[i] = new Array(size);
    }

    let cubes = multi;
    let blocks = [];
    for(let x=cubes.length-1;x>-1;x--) {
        var cube = cubes[x];
        for(let y=0;y<cube.length;y++) {
          let key = x + ':' + y;
          blocks.push(<Blocks key={key} x={x} y={y} boxSize={boxSize}/>);
        }
    }
    return blocks;
  }

  render() {
    const boxSize = this.props.boxSize;
    const cubeSize = this.props.cubeSize;
    const parking = this.cube(cubeSize, boxSize);
    return <div>{parking}</div>
  }

}
