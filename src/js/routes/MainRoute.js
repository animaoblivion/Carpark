import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Parking from '../pages/parking/index';

export default class Index extends React.Component {
  render() {
    return <Switch>
      <Route path='/carpark' component={Parking}/>
    </Switch>
  }
}
